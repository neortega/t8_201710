package model.data_structures;

public class NodoSencillo <T> {
	
	private T objeto;
	
	private NodoSencillo<T> siguiente;
	
	/**
	 * Crea un nuevo nodo vac�o
	 */
	public NodoSencillo(T pObjeto)
	{
		siguiente = null;
		objeto = pObjeto;
	}
	
	public void modificarObjeto(T pObjeto)
	{
		objeto = pObjeto;
	}
	
	public T darObjeto()
	{
		return objeto;
	}
	
	public NodoSencillo<T> darSiguiente()
	{
		return siguiente;
	}
	
	public void modificarSiguiente(NodoSencillo<T> pNodo)
	{
		siguiente = pNodo;
	}
}
