package model.data_structures;

import java.util.Iterator;

import model.data_structures.NodoSencillo;

public class ListaEncadenada<T extends Comparable<T>> implements Iterable<T> {

	/**
	 * Atributo que representa el elemento inicial de la lista
	 */
	private NodoSencillo<T> list;

	/**
	 * Atributo que representa la cantidad de elementos que tiene la lista.
	 */
	private int size;

	/**
	 * Crea una lista encadenada vac�a
	 */
	public ListaEncadenada( )
	{
		list = null;
		size = 0;
	}
	
	/**
	 * Constructor del iterator de la lista
	 */
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>()
				{

			NodoSencillo<T> pos = null;
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if(size==0)
				{
					return false;
				}
				if(pos == null)
				{
					return true;
				}
			
				return pos.darSiguiente()!=null;
			}

			public T next() {
				if(pos==null)
				{
					pos = list;
				}
				else{
					pos = pos.darSiguiente();
				}
				return pos.darObjeto();
			}			
				};
	}

	/**
	 * Agrega un elemento al final de la lista
	 * @param elem elemento que se desea agregar != null
	 */
	public void agregarElemento(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
		if (list == null)
			list = nuevo;

		NodoSencillo<T> ref = list;
		while(ref!=null)
		{
			int cmp = nuevo.darObjeto().compareTo(ref.darObjeto());
			
			if(cmp > 0)
			{
				if(ref.darSiguiente()!=null)
					ref = ref.darSiguiente();
				else
					ref.modificarSiguiente(nuevo);
			}
			else if(cmp < 0)
			{
				NodoSencillo<T> temp = ref;
				ref = nuevo;
				nuevo = temp;
				ref = ref.darSiguiente();
			}
			else
			{
				if(ref.darSiguiente()!=null)
					ref = ref.darSiguiente();
				else
				{
					ref.modificarSiguiente(nuevo);
				}
			}
		}
		size ++;
	}

	/**
	 * 
	 */
	public NodoSencillo<T> darCabeza()
	{
		return list;
	}
	/**
	 * Retorna el elemento de la posicion dada por parametro de la lista
	 * @param pos posicion donde se encuentra el elemento que se desea retornar
	 * @return elemento de la posicion ingresada
	 */
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		
		if(list == null)
			return null;
		
		NodoSencillo<T> ref = list;
		
		if(pos == 0 && ref != null)
			return ref.darObjeto();

		else
		{
			for(int i = 0; ref != null && i<pos; i++)
				ref = ref.darSiguiente();
			
			return ref.darObjeto();
		}
	}


	/**
	 * Retorna el tama�o de la lista
	 */
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	/**
	 * Retorna el elemento del nodo actual
	 * @return elem - elemento de la posicion actual 
	 */
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		T elem = null;
		
		if(list == null)
			return null;
		
		NodoSencillo<T> ref = list;
		if(ref != null)
			elem = ref.darObjeto();

		return elem;
	}

	/**
	 * Avanza una posicion teniendo como referencia el nodo actual
	 * @return sePudo: true si se pudo realizar la accion; false si no se logro
	 */
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(list == null)
			return false;
		
		NodoSencillo<T> ref = list;
		
		if(ref.darSiguiente() == null)
			return false;
		
		else
		{
			ref = ref.darSiguiente();
			return true;
		}
	}

	/**
	 * Retrocede una posicion en la lista teniendo como referencia el nodo actual
	 * @return sePudo: true si se logro realizar la operacion; false si no
	 */
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		while(list.darSiguiente() != null)
		{
			NodoSencillo<T> siguiente = list.darSiguiente();
			if(siguiente.darSiguiente() == null){
				list = siguiente;
				return true;
			}
			else
				list = list.darSiguiente();
		}

		return false;
	}

	public T eliminarObjeto(int pos)
	{
		T objeto = null;
		
		if(list == null)
			return null;
		
		NodoSencillo<T> ref = list;
		
		if(pos == 0)
		{
			objeto = ref.darObjeto();
			list = ref.darSiguiente();
			size--;
			return objeto;
		}
		
		for(int i = 0; ref != null && i < pos-1; i++)
			ref = ref.darSiguiente();
	
		if(ref == null || ref.darSiguiente() == null)
			return null;
		
		objeto = ref.darSiguiente().darObjeto();
		
		NodoSencillo<T> siguiente = ref.darSiguiente();
		NodoSencillo<T> siguienteSiguiente = siguiente.darSiguiente();
		NodoSencillo<T> nuevoSiguiente = ref.darSiguiente().darSiguiente();
		
		siguiente = nuevoSiguiente;
		siguienteSiguiente = null;
		
		size--;
		
		return objeto;
	}
	
	public boolean buscarElemento (T a)
	{
		boolean respuesta = false;
		NodoSencillo<T> head = list;
		while (head.darSiguiente()!=null) {
			if(head.darObjeto()==a)
			{
				respuesta = true;
			}
			head = head.darSiguiente();

		}
		return respuesta;
	}
	
	public T buscarObjeto(T a)
	{
		if(list == null)
			return null;
		
		T objeto = null;
		NodoSencillo<T> ref = list;
		while(ref != null)
		{
			if(a.compareTo(ref.darObjeto())==0)
				objeto = ref.darObjeto();
			else
				ref = ref.darSiguiente();
			
		}
		return objeto;
	}
	public boolean isEmpty()
	{
		if(list == null)
			return false;
		
		return darNumeroElementos()==0;
	}
	
	public boolean contains(T objeto)
	{
		return buscarObjeto(objeto)!=null;
	}
}
