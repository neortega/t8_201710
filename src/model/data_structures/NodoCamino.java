package model.data_structures;

public class NodoCamino <K> 
{
	private K idFinal;
	
	private K idAdy;
	
	private double peso;
	
	private int longitud;
	
	public NodoCamino(K idFinal, K idAdy, double peso, int longitud)
	{
		this.idFinal = idFinal;
		this.idAdy = idAdy;
		this.peso = peso;
		this.longitud = longitud;
	}

	public K getIdFinal() {
		return idFinal;
	}

	public void setIdFinal(K idFinal) {
		this.idFinal = idFinal;
	}

	public K getIdAdy() {
		return idAdy;
	}

	public void setIdAdy(K idAdy) {
		this.idAdy = idAdy;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	
	

}
