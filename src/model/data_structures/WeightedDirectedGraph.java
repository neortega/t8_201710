package model.data_structures;

import java.util.Iterator;

public class WeightedDirectedGraph<K extends Comparable<K>, V> implements IWeightedDirectedGraph<K, V> {

	private int v;

	private int e;

	private ListaEncadenada<Vertice<K,V>> vertices;

	public WeightedDirectedGraph()
	{
		v =0;
		e=0;
		vertices = new ListaEncadenada<Vertice<K,V>>();
	}


	@Override
	public int numVertices() {
		return v;
	}

	@Override
	public V darVertice(K id) {
		V buscado = null;
		for (Vertice<K,V> actual : vertices) {
			if(actual.getId()==id)
			{
				buscado= actual.getInfo();
			}
		}
		return buscado;
	}

	@Override
	public void agregarVertice(K id, V infoVer) {

		ListaEncadenada<Arco<K>> nuevaLista = new ListaEncadenada<>();
		Vertice nuevo = new Vertice(id, infoVer, nuevaLista);
		if(!vertices.contains(nuevo))
		{
			vertices.agregarElemento(nuevo);
		}
	}

	@Override
	public int numArcos() {
		return e;
	}

	@Override
	public double darPesoArco(K idOrigen, K idDestino) {
		double respuesta =0.0;
		for (Vertice<K,V> actual : vertices) {
			if(actual.getId()==idOrigen)
			{
				ListaEncadenada<Arco<K>> listaArcos = actual.getArcos();
				for (Arco<K> arco : listaArcos) {
					if(arco.other()== idDestino)
					{
						respuesta= arco.peso();
					}
				}
			}
		}
		return respuesta;
	}

	@Override
	public void agregarArco(K idOrigen, K idDestino, double peso) {
		boolean existeOrigen = false;
		boolean existeDestino = false;
		for (Vertice<K,V> actual : vertices) {
			if(actual.getId()==idOrigen)
			{
				existeOrigen= true;
			}
			if(actual.getId()==idDestino)
			{
				existeDestino= true;
			}
		}
		if(existeDestino && existeOrigen)
		{
			for (Vertice<K,V> actual : vertices) {
				if(actual.getId()==idOrigen)
				{
					Arco<K> nuevo = new Arco<K>( idDestino , 'z', peso);
					actual.getArcos().agregarElemento(nuevo);
				}
			}
		}
	}

	@Override
	public void agregarArco(K idOrigen, K idDestino, double peso,
			char ordenLexicografico) {
		boolean existeOrigen = false;
		boolean existeDestino = false;
		for (Vertice<K,V> actual : vertices) {
			if(actual.getId()==idOrigen)
			{
				existeOrigen= true;
			}
			if(actual.getId()==idDestino)
			{
				existeDestino= true;
			}
		}
		if(existeDestino && existeOrigen)
		{
			for (Vertice<K,V> actual : vertices) {
				if(actual.getId()==idOrigen)
				{
					Arco<K> nuevo = new Arco<K>( idDestino , ordenLexicografico, peso);
					actual.getArcos().agregarElemento(nuevo);
				}
			}
		}

	}

	@Override
	public Iterator<K> darVertices() {
		ListaEncadenada<K> idVertices = new ListaEncadenada<K>();
		for (Vertice<K,V> actual : vertices) {
			idVertices.agregarElemento(actual.getId());
		}
		return idVertices.iterator();
	}

	@Override
	public int darGrado(K id) {
		int respuesta = 0;
		for (Vertice<K,V> actual : vertices) {
			if(actual.getId()==id)
			{
				respuesta= actual.getArcos().darNumeroElementos();
			}
		}
		return respuesta;
	}

	@Override
	public Iterator<K> darVerticesAdyacentes(K id) {
		ListaEncadenada<K> idVerticesAdyacentes = new ListaEncadenada<K>();
		for (Vertice<K,V> actual : vertices) {
			if(actual.getId()==id)
			{
				ListaEncadenada<Arco<K>> listaArcos = actual.getArcos();
				for (Arco<K> arco : listaArcos) {
					idVerticesAdyacentes.agregarElemento(arco.other());
				}
			}
		}
		return idVerticesAdyacentes.iterator();
	}

	@Override
	public void desmarcar() {
		// TODO Auto-generated method stub

	}

	@Override
	public NodoCamino<K>[] DFS(K idOrigen) {
		// TODO Auto-generated method stub
		NodoCamino<K>[] caminos = (NodoCamino<K>[]) new NodoCamino[v];
		
		
		
		
		return caminos;
	}

	@Override
	public NodoCamino<K>[] BFS(K idOrigen) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodoCamino<K>[] darCaminoDFS(K idOrigen, K idDestino) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodoCamino<K>[] darCaminoBFS(K idOrigen, K idDestino) {
		// TODO Auto-generated method stub
		return null;
	}

	class DepthFirstSearch
	{
		public boolean[] marked;
		
		public DepthFirstSearch(WeightedDirectedGraph<K,V> WDG , int v, ListaEncadenada<Vertice<K,V>> listaVerticesMarcados)
		{
			marked = new boolean[WDG.v];
			dfs(listaVerticesMarcados, WDG, v);
		}
		public DepthFirstSearch(WeightedDirectedGraph<K,V> WDG, Iterable<K> ids, ListaEncadenada<Vertice<K,V>> listaVerticesMarcados)
		{
			marked = new boolean[WDG.v];
			for(K id : ids)
			{
				int pos = (Integer) id;
				
				if(!marked[pos])
					dfs(listaVerticesMarcados,WDG, pos);
			}
			
		}
		private void dfs(ListaEncadenada<Vertice<K,V>> listaVerticesMarcados, WeightedDirectedGraph<K,V> WDG , int v)
		{
			marked[v] = true;
			
		}
		public boolean estaMarcado(int v)
		{
			return marked[v];
		}
	}
	
}
