package model.data_structures;

import java.util.Iterator;

public interface IWeightedDirectedGraph<K extends Comparable<K>,V> {
	
	/**
	 * Retorna el n�mero de vertices
	 */
	public int numVertices();
	
	/**
	 * Consulta la informaci�n de un v�rtice dado su identificador. Si no existe el identificador se retorna
	 * la excepci�n "NoSuchElementException"
	 * @param id - Identificador del v�rtice
	 * @return vertice cuyo identificador es equivalente al enviado por par�metro
	 */
	public V darVertice(K id);
	
	public void agregarVertice(K id, V infoVer);
	
	public int numArcos();
	
	public double darPesoArco(K idOrigen, K idDestino);
	
	public void agregarArco(K idOrigen, K idDestino, double peso);
	
	public void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico);
	
	public Iterator<K> darVertices();
	
	public int darGrado(K id);
	
	public Iterator<K> darVerticesAdyacentes(K id);
	
	public void desmarcar();
	
	public NodoCamino<K>[] DFS(K idOrigen);
	
	public NodoCamino<K>[] BFS(K idOrigen);
	
	public NodoCamino<K>[] darCaminoDFS(K idOrigen, K idDestino);

	public NodoCamino<K>[] darCaminoBFS(K idOrigen, K idDestino);
}
