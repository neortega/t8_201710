package model.data_structures;

public class Vertice<K extends Comparable<K>, V> implements Comparable<Vertice<K,V>> {

	private K id;
	
	private V info;
	
	private ListaEncadenada<Arco<K>> arcos;

	public Vertice(K id, V info, ListaEncadenada<Arco<K>> arcos) {
		this.id = id;
		this.info = info;
		this.arcos = arcos;
	}

	public K getId() {
		return id;
	}

	public void setId(K id) {
		this.id = id;
	}

	public V getInfo() {
		return info;
	}

	public void setInfo(V info) {
		this.info = info;
	}

	public ListaEncadenada<Arco<K>> getArcos() {
		return arcos;
	}

	public void setArcos(ListaEncadenada<Arco<K>> arcos) {
		this.arcos = arcos;
	}

	@Override
	public int compareTo(Vertice<K, V> arg0) {
		// TODO Auto-generated method stub
		if(id.compareTo(arg0.id) > 0)
			return 1;
		else if(id.compareTo(arg0.id)<0)
			return -1;
		else
			return 0;
	}
}
