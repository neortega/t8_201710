package model.data_structures;

public class Arco<K extends Comparable<K>> implements Comparable <Arco<K>> {

	private K v;

	private double peso;

	private char ordenLexico;

	public Arco(K v, char ordenLexico,  double peso)
	{
		this.v = v;
		this.peso = peso;
		this.ordenLexico = ordenLexico;
	}

	public double peso()
	{
		return peso;
	}	
	public K other()
	{
		return v;
	}

	public char ordenLexico() {
		return ordenLexico;
	}

	public void setOrdenLexico(char ordenLexico) {
		this.ordenLexico = ordenLexico;
	}

	@Override
	public int compareTo(Arco<K> arg0) 
	{

		if(this.ordenLexico>arg0.ordenLexico)
		{
			return 1;
		}
		else if(this.ordenLexico<arg0.ordenLexico)
		{
			return -1;
		}
		else
		{
			if(this.peso > arg0.peso)
				return 1;
			else if(this.peso < arg0.peso)
				return -1;
			else
				return 0;
		}
	}

}
